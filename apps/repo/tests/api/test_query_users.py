from graphene.test import Client
from snapshottest.django import TestCase as SnapshotTestCase

from django_scrapy.tests.constants import ALL_FIXTURES


class UserTestCase(SnapshotTestCase):
    fixtures = ALL_FIXTURES

    @staticmethod
    def execute_graphql(graphql_file_name, values=None):
        from django_scrapy.schema import schema
        from django_scrapy.utils.graphql import GraphQL

        graphene_client = Client(schema)
        graph_ql = GraphQL(graphql_file_name, base_path="./apps/repo/tests/api/graphql")
        executed = graphene_client.execute(graph_ql.read(), variable_values=values)
        return executed

    def test_get_users(self):
        executed = self.execute_graphql("getUsers.graphql")
        self.assertMatchSnapshot(executed)
