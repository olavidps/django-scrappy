# -*- coding: utf-8 -*-
# snapshottest: v1 - https://goo.gl/zC4yUc
from __future__ import unicode_literals

from snapshottest import Snapshot

snapshots = Snapshot()

snapshots["UserTestCase::test_get_users 1"] = {
    "data": {
        "users": [
            {
                "email": "pytest-dev@gmail.com",
                "originalId": 1,
                "repositories": [
                    {
                        "isPrivate": False,
                        "name": "Pytest",
                        "originalId": 1,
                        "url": "https://github.com/pytest-dev/pytest",
                    }
                ],
                "username": "pytest-dev",
            },
            {
                "email": "topjohnwu@yahoo.com",
                "originalId": 2,
                "repositories": [
                    {
                        "isPrivate": False,
                        "name": "Magisk",
                        "originalId": 2,
                        "url": "https://github.com/topjohnwu/Magisk",
                    },
                    {
                        "isPrivate": False,
                        "name": "jtar",
                        "originalId": 3,
                        "url": "https://github.com/topjohnwu/jtar",
                    },
                ],
                "username": "topjohnwu",
            },
            {
                "email": "google@gmail.com",
                "originalId": 3,
                "repositories": [],
                "username": "google",
            },
        ]
    }
}
