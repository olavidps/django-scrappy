from django.contrib import admin

from apps.repo.models import Repo, User


class RepoAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "user",
        "is_private",
        "has_issues",
        "has_projects",
        "language",
        "forks_count",
        "size",
        "github_id",
        "github_created_at",
        "created_at",
    )
    list_filter = ("language",)


admin.site.register(User)
admin.site.register(Repo, RepoAdmin)
