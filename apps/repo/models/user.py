from django.db import models

from django_scrapy.models.auditable import SimpleAuditableMixin


class User(SimpleAuditableMixin):
    username = models.CharField(max_length=256)
    name = models.CharField(max_length=256)
    email = models.CharField(max_length=256, null=True)
    bio = models.CharField(max_length=256, default="")
    followers = models.IntegerField(default=0)
    following = models.IntegerField(default=0)
    github_id = models.IntegerField()
    github_created_at = models.DateTimeField(null=True)
    github_updated_at = models.DateTimeField(null=True)

    def __str__(self):
        return "User %s" % self.username
