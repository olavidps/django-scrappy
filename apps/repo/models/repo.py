from django.db import models

from django_scrapy.models.auditable import SimpleAuditableMixin


class Repo(SimpleAuditableMixin):
    name = models.CharField(max_length=256)
    url = models.CharField(max_length=512)
    user = models.ForeignKey(
        "User", on_delete=models.CASCADE, null=False, related_name="repositories_set"
    )
    is_private = models.BooleanField(default=False)
    has_issues = models.IntegerField(default=0)
    has_projects = models.BooleanField(default=False)
    language = models.CharField(max_length=256, default="", null=True)
    forks_count = models.IntegerField(default=0)
    size = models.IntegerField(default=0)
    github_id = models.IntegerField()
    github_created_at = models.DateTimeField(null=True)
    github_updated_at = models.DateTimeField(null=True)

    def __str__(self):
        return "Repo %s - %s" % (self.name, self.user.username)
