import graphene
from graphene_django import DjangoObjectType

from apps.repo.models import User


class UserNode(DjangoObjectType):
    original_id = graphene.Int()
    username = graphene.String()
    email = graphene.String()
    repositories = graphene.List("apps.repo.schema.nodes.repo.RepoNode")

    class Meta:
        model = User
        only_fields = ("original_id", "username", "email")
        interfaces = (graphene.Node,)

    @staticmethod
    def resolve_original_id(root, info):
        return root.id

    @staticmethod
    def resolve_repositories(root, info):
        return root.repositories_set.all()
