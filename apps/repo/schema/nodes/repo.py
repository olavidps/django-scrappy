import graphene
from graphene_django import DjangoObjectType

from apps.repo.models import Repo


class RepoNode(DjangoObjectType):
    original_id = graphene.Int()
    name = graphene.String()
    url = graphene.String()
    is_private = graphene.Boolean()

    class Meta:
        model = Repo
        only_fields = ("name", "url", "is_private")
        interfaces = (graphene.Node,)

    @staticmethod
    def resolve_original_id(root, info):
        return root.id
