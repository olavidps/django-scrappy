import graphene

from apps.repo.models import User
from apps.repo.schema.nodes.user import UserNode


class UserQuery(graphene.ObjectType):
    users = graphene.List(UserNode)

    @staticmethod
    def resolve_users(root, info):
        return User.objects.all()
