from django.contrib import admin

from apps.scraper.models import CronTask, DataRequestLog


class DataRequestLogAdmin(admin.ModelAdmin):
    list_display = (
        "response_status_code",
        "total_user_records_in_response",
        "total_repo_records_in_response",
        "created_at",
        "updated_at",
    )
    list_filter = ("response_status_code",)


admin.site.register(DataRequestLog, DataRequestLogAdmin)
admin.site.register(CronTask)
