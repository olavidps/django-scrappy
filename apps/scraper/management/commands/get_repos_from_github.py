from django.core.management import BaseCommand

from apps.scraper.utils import DataRequest


class Command(BaseCommand):
    help = "Call GitHub API to fetch the most recent repositories"

    def add_arguments(self, parser):
        parser.add_argument("total_records_to_fetch", type=int, default=2)

    def handle(self, *args, **options):
        total_records_to_fetch = options["total_records_to_fetch"]
        DataRequest.process(total_records_to_fetch)
        self.stdout.write("Data fetched from command")
