import json
import time

import requests

from apps.repo.models import Repo, User
from apps.scraper import constants as scraper_constants
from apps.scraper.models import DataRequestLog


class DataRequest:
    @staticmethod
    def fetch_most_active_repos_from_api(total=2):
        """Fetches the GitHub API to get the last updated repositories.

        :param total: int (max 30). Quantity of repositories to fetch. 2 by default.
        :return: List of the fetched repositories, and an int with the response status code.
        """
        response = requests.get(
            "%s&per_page=%s" % (scraper_constants.GITHUB_LAST_UPDATED_REPOS_URL, total)
        )

        if response.status_code in [200]:
            response_text = json.loads(response.text)
            time.sleep(3)

            if getattr(response_text, "items"):
                repos = response_text.get("items")
                return [
                    {
                        "name": repo.get("name"),
                        "github_id": repo.get("id"),
                        "url": repo.get("html_url"),
                        "is_private": repo.get("private"),
                        "has_issues": repo.get("has_issues"),
                        "has_projects": repo.get("has_projects"),
                        "language": repo.get("language"),
                        "forks_count": repo.get("forks_count"),
                        "size": repo.get("size"),
                        "github_created_at": repo.get("created_at"),
                        "github_updated_at": repo.get("updated_at"),
                        "user_github_id": repo.get("owner").get("id"),
                        "user_username": repo.get("owner").get("login"),
                    }
                    for repo in repos
                ], response.status_code

        return [], response.status_code

    @staticmethod
    def fetch_user_data_from_api(username):
        """Fetches a specific user in the GitHub api, based in its username.

        :param username: str
        :return: dict
        """
        response = requests.get(
            "%s/%s" % (scraper_constants.GITHUB_USERS_API_URL, username)
        )
        time.sleep(2)

        if response.status_code in [200]:
            response_text = json.loads(response.text)

            if response_text.get("login", None):
                return {
                    "github_id": response_text.get("id"),
                    "username": response_text.get("login"),
                    "email": response_text.get("email"),
                    "github_created_at": response_text.get("created_at"),
                    "github_updated_at": response_text.get("updated_at"),
                }

        return {}

    @classmethod
    def process_users_to_create(cls, users_to_create, fetched_users_data_dict):
        data_to_insert = []
        for user_github_id in users_to_create:
            fetched_user_username = fetched_users_data_dict.get(user_github_id, None)
            if fetched_user_username:
                user_data = cls.fetch_user_data_from_api(fetched_user_username)
                if user_data:
                    data_to_insert.append(user_data)

        users = [User(**record) for record in data_to_insert]

        return len(User.objects.bulk_create(users))

    @classmethod
    def process_users_to_update(cls, users_to_update, fetched_users_data_dict):
        users = User.objects.filter(github_id__in=users_to_update).all()
        new_users = []

        for user in users:
            fetched_user_username = fetched_users_data_dict.get(user.github_id)
            new_user_data = cls.fetch_user_data_from_api(fetched_user_username)
            if user.github_updated_at != new_user_data.get("github_updated_at"):
                user.name = new_user_data.get("name")
                user.email = new_user_data.get("email")
                user.bio = new_user_data.get("bio")
                user.followers = new_user_data.get("followers")
                user.following = new_user_data.get("following")
                user.github_updated_at = new_user_data.get("github_updated_at")
                new_users.append(user)

        if len(new_users) > 0:
            return User.objects.bulk_update(
                new_users,
                ["name", "email", "bio", "followers", "following", "github_updated_at"],
            )

        return 0

    @classmethod
    def process_repos_to_create(
        cls,
        repos_to_create_github_ids,
        fetched_repos_data_dict,
        fetched_users_data_dict,
    ):
        users_ids_by_github_ids_dict = cls.get_users_ids_by_github_ids_dict(
            fetched_users_data_dict
        )
        updated_repos = []

        for repo_github_id in repos_to_create_github_ids:
            repo = fetched_repos_data_dict.get(repo_github_id)
            repo["user_id"] = users_ids_by_github_ids_dict.get(
                repo.get("user_github_id")
            )
            repo.pop("user_github_id", None)
            repo.pop("user_username", None)
            updated_repos.append(repo)

        records = [Repo(**repo) for repo in updated_repos]

        return len(Repo.objects.bulk_create(records))

    @staticmethod
    def process_repos_to_update(repos_to_update_github_ids, fetched_repos_data_dict):
        repos = Repo.objects.filter(github_id__in=repos_to_update_github_ids).all()

        for repo in repos:
            new_repo_data = fetched_repos_data_dict.get(repo.github_id)
            repo.name = new_repo_data.get("name")
            repo.url = new_repo_data.get("url")
            repo.is_private = new_repo_data.get("is_private")
            repo.has_issues = new_repo_data.get("has_issues")
            repo.has_projects = new_repo_data.get("has_projects")
            repo.language = new_repo_data.get("language")
            repo.forks_count = new_repo_data.get("forks_count")
            repo.size = new_repo_data.get("size")
            repo.github_updated_at = new_repo_data.get("github_updated_at")

        if len(repos) > 0:
            return Repo.objects.bulk_update(
                repos,
                [
                    "name",
                    "url",
                    "is_private",
                    "has_issues",
                    "has_projects",
                    "language",
                    "forks_count",
                    "size",
                    "github_updated_at",
                ],
            )

        return 0

    @staticmethod
    def get_users_ids_by_github_ids_dict(fetched_users_data_dict):
        """Creates a dict of Users for existing users, with key: user_github_id and the value is the stored user_id.

        :param fetched_users_data_dict: dict(user_github_id: user_username)
        :return: dict
        """
        users_github_ids = fetched_users_data_dict.keys()
        users_ids_by_github_ids_dict = dict()
        users = User.objects.filter(github_id__in=users_github_ids).values(
            "github_id", "id"
        )

        for user in users:
            users_ids_by_github_ids_dict[user.get("github_id")] = user.get("id")

        return users_ids_by_github_ids_dict

    @staticmethod
    def get_repos_fetched_data_to_dict(repos):
        """Receives a list of repositories data (dict).
        Returns two dicts with keys composed by the Repo and User's github_id.

        :param repos: List(dict)
        :return: dict, dict
        """
        repos_data_dict = dict()
        users_data_dict = dict()

        for repo in repos:
            repos_data_dict[repo.get("github_id")] = repo
            users_data_dict[repo.get("user_github_id")] = repo.get("user_username")

        return repos_data_dict, users_data_dict

    @staticmethod
    def split_data_to_create_and_update(repos):
        """Receives the fetched data and compares it with the existing data in the DB.
        Returns lists of new and existing github_id instances for Repo and User.

        :param repos: List(dict)
        :return: List(List(int), List(int), List(int), List(int))
        """
        users_github_ids = list()
        repos_github_ids = list()

        for repo in repos:
            users_github_ids.append(repo.get("user_github_id"))
            repos_github_ids.append(repo.get("github_id"))

        users_to_update = User.objects.filter(id__in=users_github_ids).values_list(
            "github_id", flat=True
        )
        repos_to_update = Repo.objects.filter(id__in=repos_github_ids).values_list(
            "github_id", flat=True
        )

        users_to_create = list(set(users_github_ids) - set(users_to_update))
        repos_to_create = list(set(repos_github_ids) - set(repos_to_update))

        return users_to_create, users_to_update, repos_to_create, repos_to_update

    @classmethod
    def get_and_process_data(cls, total=2):
        """Calls the methods to use the GitHub repositories api.
        Separates the fetched data in new and existing data.
        For each data (new, existing) a method is executed.

        :param total: int (max 30). Quantity of repositories to fetch. 2 by default.
        :return: A list of 5 ints (4 for total of updated/created records.
        And one int with the main api response satus code.
        """
        fetched_repos, response_status_code = cls.fetch_most_active_repos_from_api(
            total
        )
        (
            users_to_create,
            users_to_update,
            repos_to_create,
            repos_to_update,
        ) = cls.split_data_to_create_and_update(fetched_repos)
        (
            fetched_repos_data_dict,
            fetched_users_data_dict,
        ) = cls.get_repos_fetched_data_to_dict(fetched_repos)

        return (
            cls.process_users_to_create(users_to_create, fetched_users_data_dict),
            cls.process_users_to_update(users_to_update, fetched_users_data_dict),
            cls.process_repos_to_create(
                repos_to_create, fetched_repos_data_dict, fetched_users_data_dict
            ),
            cls.process_repos_to_update(repos_to_update, fetched_repos_data_dict),
            response_status_code,
        )

    @classmethod
    def process(cls, total=2):
        """Fetches the most recent repositories from the GitHub API.
        Saves or updates the User, Repo records with the obtained data, depending on if the data is new or exists.

        :param total: int (max 30). Quantity of repositories to fetch. 2 by default.
        :return: True if there is success with the task, otherwise return False.
        """
        request_log = DataRequestLog.objects.create(
            total_user_records_in_response=0,
            total_repo_records_in_response=0,
        )

        try:
            (
                total_created_users,
                total_updated_users,
                total_created_repos,
                total_updated_repos,
                response_status_code,
            ) = cls.get_and_process_data(total)

            request_log.response_status_code = response_status_code
            request_log.total_user_records_in_response = (
                total_created_users + total_updated_users
            )
            request_log.total_repo_records_in_response = (
                total_created_repos + total_updated_repos
            )

            request_log.save()

            return request_log

        except Exception as e:
            print("There was an error: %s" % str(e))

        return None
