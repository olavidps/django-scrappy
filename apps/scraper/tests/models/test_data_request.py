from django.test import TestCase

from apps.scraper.utils import DataRequest


class DataRequestTestCase(TestCase):
    def test_get_most_active_repos(self):
        quantity_of_repos_to_fetch = 3
        data_request_log = DataRequest.process(quantity_of_repos_to_fetch)
        self.assertEqual(data_request_log.response_status_code, 200)
        self.assertEqual(
            data_request_log.total_user_records_in_response, quantity_of_repos_to_fetch
        )
        self.assertEqual(
            data_request_log.total_repo_records_in_response, quantity_of_repos_to_fetch
        )
