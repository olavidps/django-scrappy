GITHUB_LAST_UPDATED_REPOS_URL = (
    "https://api.github.com/search/repositories?q=python&sort=updated"
)

GITHUB_USERS_API_URL = "https://api.github.com/users"
