from datetime import datetime as dt
from time import sleep

from celery import shared_task
from celery.utils.log import get_task_logger
from django.core.management import call_command
from django.utils import timezone

from apps.scraper.models import CronTask, DataRequestLog

logger = get_task_logger(__name__)


@shared_task
def scraper_task():
    sleep(5)
    cron_tasks = CronTask.objects.all()
    for cron_task in cron_tasks:
        if task_should_be_run(cron_task):
            call_command(cron_task.command, cron_task.total_records_to_fetch)
            logger.info("%s executed" % cron_task.command)


def task_should_be_run(cron_task):

    exists_executed_task_log = DataRequestLog.objects.all().exists()
    if not exists_executed_task_log:
        return True

    time_difference = (
        dt.now(timezone.utc)
        - DataRequestLog.objects.all().order_by("-created_at").first().created_at
    )
    return (time_difference.seconds // 60) % 60 >= cron_task.minutes
