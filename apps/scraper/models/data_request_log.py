from django.db import models

from django_scrapy.models.auditable import SimpleAuditableMixin


class DataRequestLog(SimpleAuditableMixin):
    response_status_code = models.SmallIntegerField(null=True)
    total_user_records_in_response = models.SmallIntegerField(default=0)
    total_repo_records_in_response = models.SmallIntegerField(default=0)
