from apps.scraper.models.cron_tasks import CronTask
from apps.scraper.models.data_request_log import DataRequestLog

__ALL__ = ["DataRequestLog", "CronTask"]
