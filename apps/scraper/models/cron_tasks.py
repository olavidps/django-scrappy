from django.db import models

from django_scrapy.models.auditable import SimpleAuditableMixin


class CronTask(SimpleAuditableMixin):
    COMMAND_GET_REPOS_FROM_GITHUB = "get_repos_from_github"
    AVAILABLE_COMMANDS = [
        (COMMAND_GET_REPOS_FROM_GITHUB, "Get repositories from GitHub")
    ]
    minutes = models.SmallIntegerField()
    command = models.CharField(
        max_length=256, choices=AVAILABLE_COMMANDS, null=False, unique=True
    )
    total_records_to_fetch = models.SmallIntegerField()
