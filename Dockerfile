FROM python:3.9-alpine

# Set work directory
WORKDIR /usr/src/app

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# install psycopg2 dependencies
RUN apk update && apk add postgresql-dev gcc python3-dev musl-dev

# Install dependencies
COPY ./requirements/ /tmp/requirements/
RUN pip install --upgrade pip
RUN pip install -r /tmp/requirements/dev.txt

# Copy project
COPY . .