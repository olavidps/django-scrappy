class GraphQL:
    def __init__(self, graphql_file, base_path=None):
        self.graphql_file = graphql_file
        self.base_path = base_path

    def read(self):
        graph_file = self.open_file(self.graphql_file)
        graph_query = graph_file.read()
        self.close_file(graph_file)
        return graph_query

    def open_file(self, path):
        path_file = "%s/%s" % (self.base_path, path)
        file_opened = open(path_file, "rt")
        return file_opened

    @staticmethod
    def close_file(file_opened):
        file_opened.close()
