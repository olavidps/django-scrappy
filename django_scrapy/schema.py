import graphene
from apps.repo.schema.queries.user import UserQuery


class Query(UserQuery,):
    pass


schema = graphene.Schema(query=Query)