
USER_FIXTURES = [
    'apps/repo/fixtures/user.json',
]

REPO_FIXTURES = [
    'apps/repo/fixtures/repo.json',
]

ALL_FIXTURES = (
    USER_FIXTURES + REPO_FIXTURES
)
